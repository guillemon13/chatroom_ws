Chatroom in NodeJS
---------------------

This is the implementation of a Chatroom using the NodeJS technology.
It consists in a server listening at port 3300, and some clients.

The server broadcasts all the messages.

We keep to you how we implemented it.
However, here it comes the reference:

http://code.tutsplus.com/tutorials/real-time-chat-with-nodejs-socket-io-and-expressjs--net-31708

Have fun!
Guille, Rodrigo & Valentin.
